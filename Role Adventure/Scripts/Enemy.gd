extends KinematicBody2D

onready var player = get_node("/root/Level 1/Player")
onready var animator = get_node("AnimatedSprite")
onready var bar = get_node("ProgressBar")

var velocity = Vector2()
export (int) var leftBorder = -300
export (int) var rightBorder = 300
export (int) var upBorder = -100
export (int) var downBorder = 100
export (int) var health = 50
export (int) var damage = 1
export (int) var speed = 50

var bar1
var bar2
var bar3

func _ready():
	if self.name.begins_with("Creep") or self.name.begins_with("@Creep"):
		self.bar.max_value = health
		self.bar.min_value = 0
		self.bar.value = health

	if self.name == "Boss":
		self.bar.max_value = health
		self.bar.min_value = 1200
		self.bar.value = health
		
		bar1 = get_node("ProgressBar2")
		bar1.max_value = 1200
		bar1.min_value = 800
		bar1.value = bar1.max_value
		
		bar2 = get_node("ProgressBar3")
		bar2.max_value = 800
		bar2.min_value = 400
		bar2.value = bar2.max_value
		
		bar3 = get_node("ProgressBar4")
		bar3.max_value = 400
		bar3.min_value = 0
		bar3.value = bar3.max_value

func move():
	if player.position.x > position.x:
		velocity.x += speed
	if player.position.x < position.x:
		velocity.x -= speed
	if player.position.y > position.y:
		velocity.y += speed
	if player.position.y < position.y:
		velocity.y -= speed

func _physics_process(delta):
	velocity = Vector2()
	if player.position.x <= rightBorder and player.position.x >= leftBorder and player.position.y <= downBorder and player.position.y >= upBorder:
		move()
	velocity = move_and_slide(velocity)
	
func _process(delta):
	if self.name.begins_with("Creep"):
		self.bar.value = health
		
	if self.name == "Boss":
		self.bar.value = health
		self.bar1.value = health
		self.bar2.value = health
		self.bar3.value = health
		
	if health <= 0:
		get_parent().remove_child(self)
		self.visible = false
		self.get_node("CollisionShape2D").disabled = true
	if velocity.x != 0 or velocity.y != 0:
		animator.play("move")
		if velocity.x > 0:
			animator.flip_h = false
		else:
			animator.flip_h = true
	else:
		animator.play("idle")
