extends Area2D

const FIREBALL_SPEED = 200
onready var particle = get_node("Particles2D")
var direction = 'right_attack'

func _ready():
	pass

func fire(facing, position):
	particle.one_shot = true
	if get_parent() == get_node("/root"):
		particle.emitting = false
		monitorable = false
		return
	self.direction = facing
	self.position.x = position.x
	self.position.y = position.y

func _physics_process(delta):
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.is_in_group("enemies"):
			body.health -= (PlayerVariables.damage*3)

func _process(delta):
	var move = Vector2()
	if direction == 'right_attack':
		move.x += FIREBALL_SPEED
	elif direction == 'left_attack':
		move.x -= FIREBALL_SPEED
	elif direction == 'up_attack':
		move.y -= FIREBALL_SPEED
	elif direction == 'down_attack':
		move.y += FIREBALL_SPEED
	self.position.x += (move.x * delta)
	self.position.y += (move.y * delta)