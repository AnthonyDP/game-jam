extends MarginContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _draw():
	draw_set_transform(Vector2(16, 16), 0, Vector2(0.75, 0.75))
	_draw_cooldown()
	draw_set_transform(Vector2(16, 16), 0, Vector2(2, 2))
	for x in range(PlayerVariables.max_health/100):
		if x < (PlayerVariables.health/100):
			draw_texture(preload("res://Assets/Creator1/0x72_DungeonTilesetII_v1.3/frames/ui_heart_full.png"), Vector2(16+x*10, 16))
		else:
			draw_texture(preload("res://Assets/Creator1/0x72_DungeonTilesetII_v1.3/frames/ui_heart_empty.png"), Vector2(16+x*10, 16))

func _draw_cooldown():
	for x in range(30):
		if (PlayerVariables.special_used and PlayerVariables.special_ready):
			if x == 0:
				draw_texture(preload("res://Assets/Creator3/UI Adventure Pack/PNG/barBlue_horizontalLeft.png"), Vector2(42, 90))
			elif x == 29:
				draw_texture(preload("res://Assets/Creator3/UI Adventure Pack/PNG/barBlue_horizontalRight.png"), Vector2(42+x*9, 90))
			else:
				draw_texture(preload("res://Assets/Creator3/UI Adventure Pack/PNG/barBlue_horizontalBlue.png"), Vector2(42+x*9, 90))
		elif (PlayerVariables.special_used and !PlayerVariables.special_ready):
			if x == 0:
				draw_texture(preload("res://Assets/Creator3/UI Adventure Pack/PNG/barRed_horizontalLeft.png"), Vector2(42, 90))
			elif x == 29.0:
				draw_texture(preload("res://Assets/Creator3/UI Adventure Pack/PNG/barRed_horizontalRight.png"), Vector2(42+x*9, 90))
			else:
				draw_texture(preload("res://Assets/Creator3/UI Adventure Pack/PNG/barRed_horizontalMid.png"), Vector2(42+x*9, 90))
		elif (x + PlayerVariables.timerCooldownSpecial.get_time_left()) < 29.1:
			if x == 0:
				draw_texture(preload("res://Assets/Creator3/UI Adventure Pack/PNG/barGreen_horizontalLeft.png"), Vector2(42, 90))
			elif x == 29.0:
				draw_texture(preload("res://Assets/Creator3/UI Adventure Pack/PNG/barGreen_horizontalRight.png"), Vector2(42+x*9, 90))
			else:
				draw_texture(preload("res://Assets/Creator3/UI Adventure Pack/PNG/barGreen_horizontalMid.png"), Vector2(42+x*9, 90))
		else:
			if x == 0:
				draw_texture(preload("res://Assets/Creator3/UI Adventure Pack/PNG/barYellow_horizontalLeft.png"), Vector2(42, 90))
			elif x == 29.0:
				draw_texture(preload("res://Assets/Creator3/UI Adventure Pack/PNG/barYellow_horizontalRight.png"), Vector2(42+x*9, 90))
			else:
				draw_texture(preload("res://Assets/Creator3/UI Adventure Pack/PNG/barYellow_horizontalMid.png"), Vector2(42+x*9, 90))

func _process(delta):
	update()