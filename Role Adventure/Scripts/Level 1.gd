extends Node2D

var boss = preload("res://Scenes/Characters/Enemies/Boss.tscn")
var creeep = preload("res://Scenes/Characters/Enemies/Creep.tscn")
var completeCreep = true
var timerNewCreep = Timer.new()
var creepParent = Node2D.new()

func _ready():
	var player_character = get_node("/root/SelectCharacter")
	var scene
	if player_character.selectedCharacter == "Knight":
		scene = load("res://Scenes/Characters/Player/PlayerKnight.tscn")
	if player_character.selectedCharacter == "Wizzard":
		scene = load("res://Scenes/Characters/Player/PlayerWizzard.tscn")
	if player_character.selectedCharacter == "Lizard":
		scene = load("res://Scenes/Characters/Player/PlayerLizard.tscn")
	if player_character.selectedCharacter == "Berserk":
		scene = load("res://Scenes/Characters/Player/PlayerBerserk.tscn")
	if player_character.selectedCharacter == "Thief":
		scene = load("res://Scenes/Characters/Player/PlayerThief.tscn")
	
	var player = scene.instance()
	player.set_name("Player")
	player.position = Vector2(920, 60)
	add_child(player)
	
	var bossInstance = boss.instance()
	bossInstance.set_name("Boss")
	bossInstance.position = Vector2(130, 130)
	add_child(bossInstance)

	timerNewCreep.set_wait_time(3)
	timerNewCreep.connect("timeout",self,"_on_creep_died") 
	self.add_child(timerNewCreep)
	creepParent.set_name("creepParent")
	add_child(creepParent)
	for i in range(4):
		var creepInstance = creeep.instance()
		var name = str("Creep" + str(i))
		creepInstance.set_name(name)
		creepInstance.position = Vector2(500+i*20, 500+i)
		creepParent.add_child(creepInstance)
		
func _on_creep_died():
	for i in range(4-creepParent.get_child_count()):
		var creepInstance = creeep.instance()
		var name = str("Creep" + str(i))
		print(name)
		creepInstance.set_name(name)
		creepInstance.position = Vector2(500+i*20, 500+i)
		creepParent.add_child(creepInstance)

func _process(delta):
	if creepParent.get_child_count() < 4 and completeCreep:
		print("creepKurang")
		completeCreep = false
		timerNewCreep.start()
	if get_node("Boss") == null:
		get_tree().change_scene("res://Scenes/UI/Win Screen.tscn")
	if get_node("Player") == null:
		get_tree().change_scene("res://Scenes/UI/Lose Screen.tscn")
