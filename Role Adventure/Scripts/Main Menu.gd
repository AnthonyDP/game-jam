extends MarginContainer

func _ready():
	var x = preload("res://Scenes/Characters/Player/PlayerBerserk.tscn")
	var scene = x.instance()
	scene.set_name("Player")
	PlayerVariables.animator = scene.get_node("AnimatedSprite")
	PlayerVariables.weapon = scene.get_node("Weapon")
	
func _on_Play_pressed():
	get_tree().change_scene("res://Scenes/UI/Select Character.tscn")
