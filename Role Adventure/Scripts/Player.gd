extends KinematicBody2D

export (int) var speed = 200
export (int) var damage = 1
export (int) var max_health = 500
onready var animator = self.get_node("AnimatedSprite")
onready var weapon = self.get_node("Weapon")
onready var fireball = preload("res://Scenes/Characters/Fireball.tscn")

enum { IDLE, ATTACK }
var state = null
var health = max_health
var velocity = Vector2()
var timerCooldownSpecial = Timer.new()
var timerSpecial = Timer.new()
var special_ready = false
var special_used = false
var attack_direction = ['right_attack', 'down_attack', 'left_attack', 'up_attack']
var attack = 'right_attack'
var facing = Vector2()

func _ready():
	PlayerVariables.speed = speed
	PlayerVariables.damage = damage
	PlayerVariables.max_health = max_health
	PlayerVariables.health = max_health
	PlayerVariables.weapon = weapon
	PlayerVariables.special_ready = false
	PlayerVariables.special_used = false
	_state_change(IDLE)
	
	PlayerVariables.timerCooldownSpecial = timerCooldownSpecial
	PlayerVariables.timerSpecial = timerSpecial
	
	if !PlayerVariables.timerCooldownSpecial.is_stopped():
		PlayerVariables.timerCooldownSpecial.stop()
	
	PlayerVariables.timerSpecial.set_wait_time(10)
	PlayerVariables.timerSpecial.set_one_shot(true)
	PlayerVariables.timerSpecial.connect("timeout",self,"_on_special_cast") 
	add_child(PlayerVariables.timerSpecial)
	
	PlayerVariables.timerCooldownSpecial.set_wait_time(30)
	PlayerVariables.timerCooldownSpecial.set_one_shot(true)
	PlayerVariables.timerCooldownSpecial.connect("timeout",self,"_on_cooldown_ready") 
	add_child(PlayerVariables.timerCooldownSpecial)
	PlayerVariables.timerCooldownSpecial.start() 

func _on_cooldown_ready():
	PlayerVariables.special_ready = true
	print("special ready")

func _on_special_cast():
	print("special used")
	if SelectCharacter.selectedCharacter == "Knight":
		PlayerVariables.speed = 100
	if SelectCharacter.selectedCharacter == "Wizzard":
		PlayerVariables.speed = 100
	if SelectCharacter.selectedCharacter == "Thief":
		PlayerVariables.damage = 1
	if SelectCharacter.selectedCharacter == "Lizard":
		PlayerVariables.damage = 3
	if SelectCharacter.selectedCharacter == "Berserk":
		PlayerVariables.damage = 5
	PlayerVariables.special_ready = false

func _state_change(new_state):
	state = new_state

func get_input():
	velocity = Vector2()
	var move_direction = Vector2()
	
	var LEFT = Input.is_action_pressed('ui_left')
	var RIGHT = Input.is_action_pressed('ui_right')
	var UP = Input.is_action_pressed('ui_up')
	var DOWN = Input.is_action_pressed('ui_down')
	
	if RIGHT:
		velocity.x += PlayerVariables.speed
	if LEFT:
		velocity.x -= PlayerVariables.speed
	if UP:
		velocity.y -= PlayerVariables.speed
	if DOWN:
		velocity.y += PlayerVariables.speed
	
	move_direction.x = int(RIGHT) - int(LEFT)
	move_direction.y = int(DOWN) - int(UP)
	
	if RIGHT or LEFT or UP or DOWN:
		facing = move_direction
	
	attack = directionToStr(facing)

	if Input.is_action_just_pressed("attack"):
		if state == IDLE:
			_state_change(ATTACK)
			weapon.attack(attack)
			if SelectCharacter.selectedCharacter == "Wizzard":
				var Fire = fireball.instance()
				get_parent().add_child(Fire)
				Fire.fire(attack, self.position)

	if Input.is_action_just_pressed("special"):
		if !PlayerVariables.special_ready or PlayerVariables.special_used:
			pass
		else:
			print("special cast")
			PlayerVariables.special_used = true
			characterSpecial()

func directionToStr(direction):
	var angle = direction.angle()
	if angle < 0:
		angle += 2 * PI
	var index = round(angle/PI * 2)
	return attack_direction[index]

func characterSpecial():
	if SelectCharacter.selectedCharacter == "Knight":
		PlayerVariables.speed = 150
		PlayerVariables.health = max_health
	if SelectCharacter.selectedCharacter == "Wizzard":
		PlayerVariables.speed = 200
	if SelectCharacter.selectedCharacter == "Thief":
		PlayerVariables.damage = 5
	if SelectCharacter.selectedCharacter == "Lizard":
		PlayerVariables.damage = 7
		PlayerVariables.health += 300
		if PlayerVariables.health > PlayerVariables.max_health:
			PlayerVariables.health = PlayerVariables.max_health
	if SelectCharacter.selectedCharacter == "Berserk":
		PlayerVariables.damage = 10
	PlayerVariables.timerSpecial.start() 

func _physics_process(delta):
	get_input()
	velocity = move_and_slide(velocity)
	
func _process(delta):
	if PlayerVariables.health <= 0:
		get_parent().remove_child(self)
	if velocity.x != 0 or velocity.y != 0:
		animator.play("move")
		if velocity.x > 0:
			animator.flip_h = false
		else:
			animator.flip_h = true
	else:
		animator.play("idle")

func _on_Weapon_attack_finished():
	_state_change(IDLE)
