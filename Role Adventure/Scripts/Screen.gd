extends MarginContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_CharacterSelect_pressed():
	get_tree().change_scene("res://Scenes/UI/Select Character.tscn")


func _on_MainMenu_pressed():
	get_tree().change_scene("res://Scenes/UI/Main Menu.tscn")
