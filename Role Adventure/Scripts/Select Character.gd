extends MarginContainer

var selectedCharacter
onready var animknight = self.get_node("VBoxContainer/VBoxContainer/HBoxContainer2/Knight")
onready var animwizzard = self.get_node("VBoxContainer/VBoxContainer/HBoxContainer2/Wizzard")
onready var animberserk = self.get_node("VBoxContainer/VBoxContainer/HBoxContainer2/Berserk")
onready var animlizard = self.get_node("VBoxContainer/VBoxContainer/HBoxContainer2/Lizard")
onready var animthief = self.get_node("VBoxContainer/VBoxContainer/HBoxContainer2/Thief")

func _ready():
	pass

func _on_ButtonWizzard_pressed():
	SelectCharacter.selectedCharacter = "Wizzard"
	animknight.playing = false
	animwizzard.playing = true
	animberserk.playing = false
	animlizard.playing = false
	animthief.playing = false

func _on_ButtonKnight_pressed():
	SelectCharacter.selectedCharacter = "Knight"
	animknight.playing = true
	animwizzard.playing = false
	animberserk.playing = false
	animlizard.playing = false
	animthief.playing = false

func _on_ButtonLizard_pressed():
	SelectCharacter.selectedCharacter = "Lizard"
	animknight.playing = false
	animwizzard.playing = false
	animberserk.playing = false
	animlizard.playing = true
	animthief.playing = false

func _on_ButtonBerserk_pressed():
	SelectCharacter.selectedCharacter = "Berserk"
	animknight.playing = false
	animwizzard.playing = false
	animberserk.playing = true
	animlizard.playing = false
	animthief.playing = false

func _on_ButtonThief_pressed():
	SelectCharacter.selectedCharacter = "Thief"
	animknight.playing = false
	animwizzard.playing = false
	animberserk.playing = false
	animlizard.playing = false
	animthief.playing = true

func _on_StartButton_pressed():
	if SelectCharacter.selectedCharacter == null:
		return
	get_tree().change_scene("res://Scenes/Level/Level 1.tscn")