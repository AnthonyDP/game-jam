extends Area2D

signal attack_finished

enum { IDLE, ATTACK }
var state = null

func _ready():
	$AnimationPlayer.connect('animation_finished', self, "_on_animation_finished")
	_change_state(IDLE, 'right_attack')


func _change_state(new_state, direction):
	match new_state:
		IDLE:
			set_physics_process(false)
			$AnimationPlayer.play('idle')
		ATTACK:
			set_physics_process(true)
			if direction == 'left_attack':
				$AnimationPlayer.play('left_attack')
			elif direction == 'right_attack':
				$AnimationPlayer.play('right_attack')
			elif direction == 'up_attack':
				$AnimationPlayer.play('up_attack')
			elif direction == 'down_attack':
				$AnimationPlayer.play('down_attack')
	state = new_state

func _physics_process(delta):
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.is_in_group("enemies"):
			body.health -= get_parent().damage

func attack(direction):
	_change_state(ATTACK, direction)

func _on_animation_finished(name):
	if name == 'idle':
		return
	_change_state(IDLE, 'right_attack')
	emit_signal("attack_finished")